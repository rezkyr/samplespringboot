FROM maven:3-jdk-8-alpine
COPY --chown=root:root entrypoint.sh entrypoint.sh
COPY target/*.jar /main.jar
ENTRYPOINT [ "/entrypoint.sh" ]