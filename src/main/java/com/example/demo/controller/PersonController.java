package com.example.demo.controller;

import com.example.demo.enitity.Person;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {

    @GetMapping(path = "/person", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Person> getPerson(@RequestParam(name = "name", defaultValue = "world") String name) {
        Person p = new Person(name);
        return ResponseEntity.ok().body(p);
    }

}